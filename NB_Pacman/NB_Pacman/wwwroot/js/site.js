﻿//import { fail } from "assert";

// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


   
var hubConnection = new signalR.HubConnectionBuilder()
        .withUrl('https://localhost:44337/pacman')
        .configureLogging(signalR.LogLevel.Information)
        .build();

var pacman = document.getElementById('pacmanImg');
var hubStopped = true;
gameManager = {
}
keyCode = 0
        
setInterval(updateServerModel, 100);



hubConnection.on("UpdateClientModel", function (serverGMModel) {

    gameManager = serverGMModel;
    if (gameManager.message)
    {
        document.getElementById('infoblock').style.display = 'table-cell';
        document.getElementById('infoblock').innerHTML = gameManager.message;
        hubConnection.stop();
        hubStopped = true;
    }
        
    UpdatePacman();
    UpdateFood();
    UpdateGhosts();

    document.getElementById('livesCount').innerHTML = gameManager.player.lives;
    document.getElementById('scoreCount').innerHTML = gameManager.player.score;
});

function UpdatePacman() {
    var d = document.getElementById('pacmanImg');

    switch (gameManager.pacman.currentDirection) {
        case 1:
            d.className = 'rotatedUp';
            break;
        case 2:
            d.className = 'rotatedRight';
            break;
        case 3:
            d.className = 'rotatedDown';
            break;
        case 4:
            d.className = 'rotatedLeft';
            break;
    }
    d.style.top = gameManager.pacman.top + "px";
    d.style.left = gameManager.pacman.left + "px";
}

function UpdateFood() {
    for (var f in gameManager.food.foodImage) {
        for (var i = 0; i < 30; i++) {
            for (var j = 0; j < 27; j++) {

                if (gameManager.food.foodImage[i][j] !== null) {
                    if (document.getElementById('block' + i + '_' + j)) {
                        if (gameManager.food.foodImage[i][j].visible) { document.getElementById('block' + i + '_' + j).style.visible = "visible" }
                        else { document.getElementById('block' + i + '_' + j).style.display = "none" }
                    }
                    else {
                        var left = gameManager.food.foodImage[i][j].left;
                        var top = gameManager.food.foodImage[i][j].top;
                        var imageUrl = gameManager.food.foodImage[i][j].imageUrl;
                        var visible = gameManager.food.foodImage[i][j].visible;

                        var iDiv = document.createElement('div');
                        iDiv.id = 'block' + i + '_' + j;
                        iDiv.className = 'food';
                        iDiv.style.left = left + 'px';
                        iDiv.style.top = top + 'px';
                        iDiv.style.visibility = 'visible';
                        iDiv.style.backgroundImage = "url('" + imageUrl + "')";
                        document.getElementById('food').appendChild(iDiv);
                    }

                }
                else {
                    if (document.getElementById('block' + i + '_' + j)) { document.getElementById('block' + i + '_' + j).style.display = "none" }
                }
            }
        }
    }
}

function UpdateGhosts() {
    for (var k = 0; k < 4; k++) {
        if (gameManager.ghost.ghostImage[k] !== null) {
            var g = gameManager.ghost.ghostImage[k];
            var div_ghost = document.getElementById('block_g' + k);
            if (div_ghost === null) {
                var gDiv = document.createElement('div');
                gDiv.id = 'block_g' + k;
                gDiv.className = 'ghost';
                gDiv.style.left = g.left + 'px';
                gDiv.style.top = g.top + 'px';
                gDiv.style.visibility = 'visible';
                gDiv.style.backgroundImage = "url('" + g.imageUrl + "')";
                document.getElementById('board').appendChild(gDiv);
            }
            else {
                div_ghost.style.left = g.left + 'px';
                div_ghost.style.top = g.top + 'px';
                div_ghost.style.visibility = 'visible';
                div_ghost.style.backgroundImage = "url('" + g.imageUrl + "')";
            }


        }
    }
}

document.addEventListener('keydown', (event) => {
    keyCode = event.keyCode;
    if (keyCode === 83) {
        hubConnection.start();
        hubStopped = false;
    }
    if (keyCode === 80) {
        hubConnection.stop();
        hubStopped = true;
    }
    updateServerModel();

});

function updateServerModel() {
    if(hubStopped == false)
        hubConnection.invoke("UpdateGameManager", gameManager, keyCode);
}


