﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NB_Pacman.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace NB_Pacman.Controllers
{
    public class HomeController : Controller
    {
        private RecordHistoryContext db;

        public HomeController(RecordHistoryContext context)
        {
            db = context;
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewData["User"] = User.Identity.Name;
            }
            else
            {
                ViewData["User"] = "анонімний гравець";
            }

            IQueryable<RecordHistory> myRecords = db.RecordsHistory.Where(r => r.UserName == User.Identity.Name);
            return View(myRecords.OrderByDescending(r => r.Score).Take(5).ToList());
        }

        [HttpPost]
        public async Task<IActionResult> Create(RecordHistory recordHistory)
        {
            db.RecordsHistory.Add(recordHistory);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Елементи керування";

            return View();
        }

        public IActionResult Records()
        {
            return View(db.RecordsHistory.OrderByDescending(r => r.Score).Take(10).ToList());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
