﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Timers;


namespace NB_Pacman
{
    public class Ghost
    {
        private const int GhostAmount = 4;
        private GameManager gm;

        public int Ghosts = GhostAmount;
        public GhostImage[] GhostImage = new GhostImage[GhostAmount];
        public int[] State = new int[GhostAmount];
       
        
        public int[] xCoordinate = new int[GhostAmount];
        public int[] yCoordinate = new int[GhostAmount];
        private int[] xStart = new int[GhostAmount];
        private int[] yStart = new int[GhostAmount];
        public int[] Direction = new int[GhostAmount];
        private Random ran = new Random();
        private bool GhostOn = false;
  
        public bool goHome; 

        public Ghost(GameManager gm)
        {
            this.gm = gm;
        }

        public void CreateGhostImage()
        {
           
            for (int x = 0; x < Ghosts; x++)
            {
                GhostImage[x] = new GhostImage();
            }
            Set_Ghosts();
            ResetGhosts();
        }

        public void Set_Ghosts()
        {
            int Amount = -1;
            for (int y = 0; y < 30; y++)
            {
                for (int x = 0; x < 27; x++)
                {
                    if (gm.field.Matrix[y, x] == 15)
                    {
                        Amount++;
                        xStart[Amount] = x;
                        yStart[Amount] = y;
                    }
                }
            }
        }

        public void ResetGhosts()
        {
            for (int x = 0; x < GhostAmount; x++)
            {
                xCoordinate[x] = xStart[x];
                yCoordinate[x] = yStart[x];
                GhostImage[x].left = xStart[x] * 16-8;
                GhostImage[x].top = yStart[x] * 16-8;
                GhostImage[x].imageUrl = "/images/Ghost" + x + ".png";
                Direction[x] = 0;
                State[x] = 0;
            }
        }

       

       

        public void updateGhost()
        {
            // Keep moving the ghosts
            for (int x = 0; x < Ghosts; x++)
            {
                if (State[x] > 1) { continue; }
                MoveGhosts(x);
            }
            GhostOn = !GhostOn;
            CheckForPacman();
            if (goHome)
                GoHome();
        }

        public void GoHome()
        {
            Set_Ghosts();
            // Move ghosts to their home positions
            for (int x = 0; x < GhostAmount; x++)
            {
                if (State[x] == 2)
                {
                    int xpos = xStart[x] * 16;
                    int ypos = yStart[x] * 16;
                    if (GhostImage[x].left > xpos) { GhostImage[x].left-=8; }
                    if (GhostImage[x].left < xpos) { GhostImage[x].left+=8; }
                    if (GhostImage[x].top > ypos) { GhostImage[x].top-=8; }
                    if (GhostImage[x].top < ypos) { GhostImage[x].top+=8; }
                    if (GhostImage[x].top == ypos && GhostImage[x].left == xpos)
                    {
                        State[x] = 0;
                        xCoordinate[x] = xStart[x];
                        yCoordinate[x] = yStart[x];
                        GhostImage[x].left = xStart[x] * 16 - 8;
                        GhostImage[x].top = yStart[x] * 16 - 8;
                        goHome = false;
                    }
                }
            }
            
        }

      
        public void killableGhost()
        {
            // Keep moving the ghosts
            for (int x = 0; x < Ghosts; x++)
            {
                if (State[x] != 1) { continue; }
                MoveGhosts(x);
            }
        }


        private void MoveGhosts(int x)
        {
            // Move the ghosts
            if (Direction[x] == 0)
            {
                if (ran.Next(0, 5) == 3) { Direction[x] = 1; }
            }
            else
            {
                bool CanMove = false;
                Other_Direction(Direction[x], x);

                while (!CanMove)
                {
                    CanMove = check_direction(Direction[x], x);
                    if (!CanMove) { Change_Direction(Direction[x], x); }

                }

                if (CanMove)
                {
                    switch (Direction[x])
                    {
                        case 1: GhostImage[x].top -= 16; yCoordinate[x]--; break;
                        case 2: GhostImage[x].left += 16; xCoordinate[x]++; break;
                        case 3: GhostImage[x].top += 16; yCoordinate[x]++; break;
                        case 4: GhostImage[x].left -= 16; xCoordinate[x]--; break;
                    }
                    switch (State[x])
                    {
                        case 0: GhostImage[x].imageUrl = "/images/Ghost" + x + ".png"; break;
                        case 1:
                            if (GhostOn) { GhostImage[x].imageUrl = "/images/GhostWhite.png"; } else { GhostImage[x].imageUrl = "/images/GhostCanBeEaten.png";};
                            break;
                        case 2: GhostImage[x].imageUrl = "/images/GhostCanBeEaten.png"; break;
                    }
                }
            }

        }

        private bool check_direction(int direction, int ghost)
        {
            switch (direction)
            {
                case 1: return direction_ok(xCoordinate[ghost], yCoordinate[ghost] - 1, ghost);
                case 2: return direction_ok(xCoordinate[ghost] + 1, yCoordinate[ghost], ghost);
                case 3: return direction_ok(xCoordinate[ghost], yCoordinate[ghost] + 1, ghost);
                case 4: return direction_ok(xCoordinate[ghost] - 1, yCoordinate[ghost], ghost);
                default: return false;
            }
        }

        private bool direction_ok(int x, int y, int ghost)
        {
            if (x < 0 || y < 0) { return false; }
            if (x < 0) { xCoordinate[ghost] = 27; GhostImage[ghost].left = 429; return true; }
            if (x > 27) { xCoordinate[ghost] = 0; GhostImage[ghost].left = -5; return true; }
            if (gm.field.Matrix[y, x] < 4 || gm.field.Matrix[y, x] > 10) { return true; } else { return false; }
        }

        private void Change_Direction(int direction, int ghost)
        {
            int which = ran.Next(0, 2);
            switch (direction)
            {
                case 1: case 3: if (which == 1) { Direction[ghost] = 2; } else { Direction[ghost] = 4; }; break;
                case 2: case 4: if (which == 1) { Direction[ghost] = 1; } else { Direction[ghost] = 3; }; break;
            }
        }

        private void Other_Direction(int direction, int ghost)
        {
            if (gm.field.Matrix[yCoordinate[ghost], xCoordinate[ghost]] < 4)
            {
                bool[] directions = new bool[5];
                int x = xCoordinate[ghost];
                int y = yCoordinate[ghost];
                switch (direction)
                {
                    case 1: case 3: directions[2] = direction_ok(x + 1, y, ghost); directions[4] = direction_ok(x - 1, y, ghost); break;
                    case 2: case 4: directions[1] = direction_ok(x, y - 1, ghost); directions[3] = direction_ok(x, y + 1, ghost); break;
                }
                int which = ran.Next(0, 5);
                if (directions[which] == true) { Direction[ghost] = which; }
            }
        }

        public void ChangeGhostState()
        {
            for (int x = 0; x < GhostAmount; x++)
            {
                if (State[x] == 0)
                {
                    State[x] = 1;
                    GhostImage[x].imageUrl = "/images/GhostCanBeEaten.png";;
                }
            }
           
        }

        public void CheckForPacman()
        {
            for (int x = 0; x < GhostAmount; x++)
            {
                if (xCoordinate[x] == gm.pacman.xCoordinate && yCoordinate[x] == gm.pacman.yCoordinate)
                {
                    switch (State[x])
                    {
                        case 0:
                            gm.player.LoseLife();
                            break;
                        case 1:
                            State[x] = 2;
                            GoHome();
                            goHome = true;
                            GhostImage[x].imageUrl = "/images/eyes.png";
                            gm.player.UpdateScore(300);
                            break;
                    }
                }
            }
        }
    }
}

public class GhostImage
{
    public int left;
    public int top;
    public string imageUrl;
    public bool visible = true;

}