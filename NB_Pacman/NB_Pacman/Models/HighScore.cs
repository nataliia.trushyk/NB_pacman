﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NB_Pacman
{
    public class HighScore
    {
        public const int InitalScore = 100;
        public int Score = InitalScore;

        public void CreateHighScore()
        {
            UpdateHighScore();
        }

        public void UpdateHighScore(int value = InitalScore)
        {
            Score = value;
        }

    }
}