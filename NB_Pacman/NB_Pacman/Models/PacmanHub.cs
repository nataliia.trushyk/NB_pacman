﻿using System;
using System.Threading;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using NB_Pacman.Models;

namespace NB_Pacman
{
    public class PacmanHub : Hub
    {
        private UserContext dbUserContext;
        private RecordHistoryContext dbRHContext;

        private static GameManager _gm;
        private int  _keyCode;

        public PacmanHub(UserContext dbUserContext,RecordHistoryContext dbRHContext)
        {
            this.dbUserContext = dbUserContext;
            this.dbRHContext = dbRHContext;

        }
        public async Task StartGame(GameManager gm, int level)
        {
            _gm = new GameManager();
            _gm.SetupGame(level);
            await Clients.All.SendAsync("UpdateClientModel", _gm);
        }       
      
        public async Task UpdateGameManager(GameManager gm, int keyCode)
        {
           
            _gm = gm;
            _keyCode = keyCode;
            if (_gm.level == 0)
            {
                _gm.SetupGame(1);
            }
            //_gm.dbRHContext = dbRHContext;
           // _gm.dbUserContext = dbUserContext;
            _gm.LastUpdatedBy = Context.ConnectionId;
            _gm.UpdateGame(keyCode);
            await Clients.All.SendAsync("UpdateClientModel", gm);
            //return Clients.Caller.SendAsync("UpdateClientModel", gm);
        }
        
    }

}
