﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NB_Pacman
{
    public class Food
    {
        public FoodImage[,] FoodImage = new FoodImage[30, 27];
        public int Amount = 0;
        //public Field field;
        private GameManager gm;
        private const int FoodScore = 10;
        private const int SuperFoodScore = 50;

        //public Player player;

        public Food(GameManager gm)
        {
            this.gm = gm;
        }

        public void CreateFoodImages()
        {
            
            for (int y = 0; y < 30; y++)
            {
                for (int x = 0; x < 27; x++)
                {
                    if (gm.field.Matrix[y, x] == 1 || gm.field.Matrix[y, x] == 2)
                    {
                        FoodImage[y, x] = new FoodImage();
                        FoodImage[y, x].left = x * 16;
                        FoodImage[y, x].top = y * 16;
                        if (gm.field.Matrix[y, x] == 1)
                        {
                            FoodImage[y, x].imageUrl = "/images/Block 1.png";
                            Amount++;
                        }
                        else
                        {
                            
                            FoodImage[y, x].imageUrl = "/images/Block 2.png";
                        }
                    }
                }
            }
        }

        public void EatFood(int x, int y)
        {
            // Eat food
            FoodImage[x, y].visible = false;
            gm.field.Matrix[x, y] = 0;
            gm.player.UpdateScore(FoodScore);
            Amount--;
            if (Amount < 1) { gm.player.LevelComplete(); }
        }

        public void EatSuperFood(int x, int y)
        {
            // Eat food
            FoodImage[x, y].visible = false;
            gm.field.Matrix[x, y] = 0;
            gm.player.UpdateScore(SuperFoodScore);
            gm.ghost.ChangeGhostState();
        }
    }

    public class FoodImage
    {
        public int left;
        public int top;
        public string imageUrl;
        public bool visible = true;

    }
}