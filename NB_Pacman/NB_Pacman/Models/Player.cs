﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NB_Pacman
{
    public class Player
    {
        private const int MaxLives = 10;
        public int Score = 0;
        public int Lives = 3;
        private GameManager gm;

        public Player(GameManager gm)
        {
            this.gm = gm;
        }
               

        public void CreatePlayerDetails()
        {
            UpdateScore(0);
        }

        public void UpdateScore(int amount = 1)
        {
            // Update score value and text
            Score += amount;
            if (Score > gm.highscore.Score) { gm.highscore.UpdateHighScore(Score); }
        }


        public void LoseLife()
        {
            // Lose a life
            Lives--;
            if (Lives > 0)
            {
                //todo повернути пакмена і привидів в поч стае
                gm.pacman.InicializeStartPosition();
                gm.pacman.Set_Pacman();
                gm.ghost.Set_Ghosts();
                gm.ghost.ResetGhosts();
            }
            else
            {
                gm.message = "Гру завершено";
            }
        }

        public void LevelComplete()
        {
            gm.message = "Рівень пройдено";
        }
    }
}