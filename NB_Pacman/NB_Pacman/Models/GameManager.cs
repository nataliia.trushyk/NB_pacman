﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NB_Pacman.Models;
using Newtonsoft.Json;

namespace NB_Pacman
{
    public class GameManager
    {
        public UserContext dbUserContext;
        public RecordHistoryContext dbRHContext;
        // We don't want the client to get the "LastUpdatedBy" property
        [JsonIgnore]
        public string LastUpdatedBy { get; set; }

        [JsonProperty("field")]
        public Field field;
        //[JsonProperty("pacman")]
        public Pacman pacman;
        [JsonProperty("food")]
        public Food food;
        public Player player;
        public HighScore highscore;
        [JsonProperty("ghost")]
        public Ghost ghost;
        public int level;
        public String message;

        public GameManager()
        {
            pacman = new Pacman(this);
            field = new Field();
            food = new Food(this);
            player = new Player(this);
            highscore = new HighScore();
            ghost = new Ghost(this);
        }

        public void SetupGame(int Level)
        {
            //Tuple<int, int> PacmanStartCoordinates = field.InitialiseBoardMatrix(Level);
            field.InitialiseBoardMatrix(Level);
            player.CreatePlayerDetails();
            highscore.CreateHighScore();
            food.CreateFoodImages();
            ghost.CreateGhostImage();
            pacman.InicializeStartPosition();
            pacman.Set_Pacman();
            //pacman.CreatePacman(PacmanStartCoordinates.Item1, PacmanStartCoordinates.Item2);
            this.level = Level;
        }

       
        public void SaveResult()
        {
            RecordHistory recordHistory = new RecordHistory();
            recordHistory.Score = player.Score;
            //var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            recordHistory.UserName = "";//User.Identity.Name;

            dbRHContext.RecordsHistory.Add(recordHistory);
            dbRHContext.SaveChangesAsync();
        }

        public void UpdateGame (int keyCode)
        {  
            pacman.UpdatePacman(keyCode);
            ghost.updateGhost();
        }

        
    }
}