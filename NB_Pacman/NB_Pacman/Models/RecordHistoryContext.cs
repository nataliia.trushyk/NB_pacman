﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace NB_Pacman.Models
{
    public class RecordHistoryContext : DbContext
    {
        public DbSet<RecordHistory> RecordsHistory { get; set; }
        public RecordHistoryContext(DbContextOptions<RecordHistoryContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
