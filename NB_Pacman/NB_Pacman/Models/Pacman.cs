﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NB_Pacman
{
    public class Pacman
    {
        // Initialise variables

        [JsonProperty("left")]
        public int left { get; set; }
        [JsonProperty("top")]
        public int top { get; set; }
       

       
        private int xStart = 0;
        private int yStart = 0;
        public int xCoordinate;
        public int yCoordinate;
        public int currentDirection = 0;
        public int nextDirection = 0;
       
        private GameManager gm;


        public Pacman(GameManager gm)
        {
            this.gm = gm;
           
            
            // Create Pacman
            //CreatePacman(PacmanStartCoordinates.Item1, PacmanStartCoordinates.Item2);
           
        }

        public void InicializeStartPosition()
        {
            
            for (int y = 0; y < 31; y++)
            {
                for (int x = 0; x < 28; x++)
                {
                    if (gm.field.Matrix[y, x] == 3) { xStart = x; yStart = y; }
                }
            }
            //Tuple<int, int> StartLocation = new Tuple<int, int>(StartX, yStart);
            //return StartLocation;
        }
        //public void CreatePacman(int StartXCoordinate, int StartYCoordinate)
        //{
        //    xStart = StartXCoordinate;
        //    yStart = StartYCoordinate;
        //    Set_Pacman();
        //}

        public void UpdatePacman(int keyCode)
        {

            switch (keyCode)
            {
                case 38: nextDirection = 1; MovePacman(1); break;
                case 39: nextDirection = 2; MovePacman(2); break;
                case 40: nextDirection = 3; MovePacman(3); break;
                case 37: nextDirection = 4; MovePacman(4); break;
            }
        }

        public void MovePacman(int direction)
        {
            // Move Pacman
            bool CanMove = check_direction(nextDirection);
            if (!CanMove) { CanMove = check_direction(currentDirection); direction = currentDirection; } else { direction = nextDirection; }
            if (CanMove) { currentDirection = direction; }

            if (CanMove)
            {
                switch (direction)
                {
                    case 1: top -= 16; yCoordinate--; break;
                    case 2: left += 16; xCoordinate++; break;
                    case 3: top += 16; yCoordinate++; break;
                    case 4: left -= 16; xCoordinate--; break;
                }
                currentDirection = direction;
                CheckPacmanPosition();
                gm.ghost.CheckForPacman();
            }
        }

        private void CheckPacmanPosition()
        {
            // Check Pacmans position
            switch (gm.field.Matrix[yCoordinate, xCoordinate])
            {
                case 1: gm.food.EatFood(yCoordinate, xCoordinate); break;
                case 2: gm.food.EatSuperFood(yCoordinate, xCoordinate); break;
            }
        }

        private bool check_direction(int direction)
        {
            // Check if pacman can move to space
            switch (direction)
            {
                case 1: return direction_ok(xCoordinate, yCoordinate - 1);
                case 2: return direction_ok(xCoordinate + 1, yCoordinate);
                case 3: return direction_ok(xCoordinate, yCoordinate + 1);
                case 4: return direction_ok(xCoordinate - 1, yCoordinate);
                default: return false;
            }
        }

        private bool direction_ok(int x, int y)
        {
            if (x < 0) { xCoordinate = 27; left = 429; return true; }
            if (x > 27) { xCoordinate = 0; left = -5; return true; }
            if (gm.field.Matrix[y, x] < 4) { return true; } else { return false; }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            // Keep moving pacman
            MovePacman(currentDirection);
        }

        public void Set_Pacman()
        {
            currentDirection = 0;
            nextDirection = 0;
            xCoordinate = xStart;
            yCoordinate = yStart;
            left = (xStart) * 16 - 8;
            top = (yStart) * 16 - 8;
        }
    }
}
