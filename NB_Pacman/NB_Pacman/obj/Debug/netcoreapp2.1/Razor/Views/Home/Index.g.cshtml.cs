#pragma checksum "C:\Users\Наталя\source\repos\NB_Pacman\NB_Pacman\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "25da362eb51f88434d4cbc67436d355c58140a34"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Наталя\source\repos\NB_Pacman\NB_Pacman\Views\_ViewImports.cshtml"
using NB_Pacman;

#line default
#line hidden
#line 2 "C:\Users\Наталя\source\repos\NB_Pacman\NB_Pacman\Views\_ViewImports.cshtml"
using NB_Pacman.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"25da362eb51f88434d4cbc67436d355c58140a34", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"33b9a68d38f9d22dc6b8a925af36339296e2a773", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<NB_Pacman.Models.RecordHistory>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\Наталя\source\repos\NB_Pacman\NB_Pacman\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
            BeginContext(97, 260, true);
            WriteLiteral(@"
<script src=""js/signalr.min.js""></script>
<script src=""js/site.js""></script>



<div id=""board"">
    <div id=""food""></div>
    <div id=""pacmanImg"">
        <img src=""/images/cA7WPuG.gif"" />
    </div>

    <div id=""playerInfo"">
        Гравець - ");
            EndContext();
            BeginContext(358, 16, false);
#line 18 "C:\Users\Наталя\source\repos\NB_Pacman\NB_Pacman\Views\Home\Index.cshtml"
             Write(ViewData["User"]);

#line default
#line hidden
            EndContext();
            BeginContext(374, 296, true);
            WriteLiteral(@"
        <div>Життів: <span id=""livesCount"">3</span></div>
        <div>Рахунок: <span id=""scoreCount"">0</span></div>
        <div id=""records"">
            <b>Мої рекорди(топ 5):</b>
            <table class=""table"">
                <tr><td>Гравець</td><td>Рахунок</td><td>Дата</td></tr>
");
            EndContext();
#line 25 "C:\Users\Наталя\source\repos\NB_Pacman\NB_Pacman\Views\Home\Index.cshtml"
                 foreach (var item in Model)
                {


#line default
#line hidden
            BeginContext(737, 28, true);
            WriteLiteral("                    <tr><td>");
            EndContext();
            BeginContext(766, 13, false);
#line 28 "C:\Users\Наталя\source\repos\NB_Pacman\NB_Pacman\Views\Home\Index.cshtml"
                       Write(item.UserName);

#line default
#line hidden
            EndContext();
            BeginContext(779, 9, true);
            WriteLiteral("</td><td>");
            EndContext();
            BeginContext(789, 10, false);
#line 28 "C:\Users\Наталя\source\repos\NB_Pacman\NB_Pacman\Views\Home\Index.cshtml"
                                              Write(item.Score);

#line default
#line hidden
            EndContext();
            BeginContext(799, 9, true);
            WriteLiteral("</td><td>");
            EndContext();
            BeginContext(809, 29, false);
#line 28 "C:\Users\Наталя\source\repos\NB_Pacman\NB_Pacman\Views\Home\Index.cshtml"
                                                                  Write(item.Date.ToShortDateString());

#line default
#line hidden
            EndContext();
            BeginContext(838, 12, true);
            WriteLiteral("</td></tr>\r\n");
            EndContext();
#line 29 "C:\Users\Наталя\source\repos\NB_Pacman\NB_Pacman\Views\Home\Index.cshtml"
                }

#line default
#line hidden
            BeginContext(869, 113, true);
            WriteLiteral("            </table>\r\n        </div>\r\n    </div>\r\n    <div id=\"infoblock\">Гру завершено</div>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<NB_Pacman.Models.RecordHistory>> Html { get; private set; }
    }
}
#pragma warning restore 1591
