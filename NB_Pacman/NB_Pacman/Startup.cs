﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NB_Pacman.Models;

namespace NB_Pacman
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

    
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //services.AddSignalR();
            services.AddSignalR(o =>
            {
                o.EnableDetailedErrors = true;
            });

            string connection = Configuration.GetConnectionString("DefaultConnection");
            //services.AddDbContext<UserContext>(options => options.UseSqlServer(connection));
            DbContextOptionsBuilder<UserContext> userContextOptions = new DbContextOptionsBuilder<UserContext>();
            userContextOptions.UseSqlServer(connection);
            services.AddSingleton(userContextOptions.Options);

            services.AddDbContext<UserContext>(options => options.UseSqlServer(connection));


            // установка конфигурации подключения
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => //CookieAuthenticationOptions
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                });

           // services.AddDbContext<RecordHistoryContext>(options =>options.UseSqlServer(connection));

            DbContextOptionsBuilder<RecordHistoryContext> dbContextOptions = new DbContextOptionsBuilder<RecordHistoryContext>();
            dbContextOptions.UseSqlServer(connection);
            services.AddSingleton(dbContextOptions.Options);

            services.AddDbContext<RecordHistoryContext>(options => options.UseSqlServer(connection));


            //services.AddScoped<IDataRepository, TokenManager>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            //app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseDefaultFiles();
            app.UseStaticFiles();


            app.UseSignalR(routes =>
            {
                routes.MapHub<PacmanHub>("/pacman");
            });


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
